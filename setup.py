"""
This file is part of PowerMonitor 
Copyright (c) Ramasubramanian S 2014
see the file license.txt for copyright permission
"""
from distutils.core import setup

def unix_install():
    import shutil
    import os
    shutil.copyfile("./powermonitor.py","/usr/bin/power-monitor")
    os.chmod("/usr/bin/power-monitor",777)


if __name__ == "__main__":
    setup ( name = 'Powermonitor',
            version = '0.4',
            description = 'simple tool to monitor the power level of the system',
            author = 'Ramasubramanian S',
            author_email = 'sramsubu@gmail.com',
            url = 'https://gitorious.org/power-monitor',
            packages = ['pm'],
        )
    import platform
    if platform.system() == "Linux":
        unix_install()
    else:
        print "Powermonitor is not implemented for your platform\nInstall Error: %s" %platform.system()

