#! /usr/bin/python

"""
Copyright (c) Ramasubramanian S 2014
See the file license.txt for copyright permission
"""

from pm import monitor
from daemon import runner

if __name__ == "__main__":
    app = monitor.Monitor()
    dr = runner.DaemonRunner(app)
    dr.do_action()
