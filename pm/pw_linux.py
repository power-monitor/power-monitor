"""
Copyright (c) 2014 Ramasubramanian S
See the file license.txt for copyright permissions.
"""
_path_="/sys/class/power_supply/BAT0/"

def get_power_level():
    fp = open(_path_+"energy_now")
    enow=int(fp.read())
    fp.close()
    fp = open(_path_+"energy_full")
    efull = int(fp.read())
    fp.close()
    power= float(enow)/float(efull)
    power = power * 100
    return power

def get_adapter_status():
    fp=open(_path_+"status")
    status = fp.read().strip()
    return status

def get_details():
    fp=open(_path_+"uevent")
    uevent=fp.readlines()
    fp.close()
    return uevent

def get_pwstatus():
    pl=get_power_level()
    status=get_adapter_status()
    if status not in ['Discharging','Charging','Full']:
        return -1
    elif status == "Discharging" and pl < 7:
        return 1
    elif status == "Full":
        return 2
    return 0


