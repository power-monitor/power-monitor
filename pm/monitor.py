"""
Copyright (c) 2014 Ramasubramanian S
See the file license.txt for copyright permission. 
"""

import pw_linux as power
import pynotify
from time import sleep
from daemon import runner
import sys

class SysTray:
    def __init__(self):
        if not pynotify.init("PM"):
            print "Cannot init pynotify"
            exit(1)
        self.n=pynotify.Notification("mynotify ")
        self.n.show()

    def notify(self,msg):
        self.n.update("PM: %s" %msg)
        self.n.show()

def monitor():
    obj = SysTray()
    obj.notify("Power monitor started")
    while(True):
        pl=power.get_power_level()
        status=power.get_adapter_status()
        if status not in ['Discharging','Charging','Full']:
            obj.notify("Unexpected adapter status: %s" %status)
            status = "Quit"
            break
        elif pl <= 10 and status == "Discharging":
            obj.notify("Battery low. Plugin charger")
        elif pl == 100 and status == "Full":
            obj.notify("Battery Full. Unplug charger and save power.")
        else:
            pass
        sleep(30)

    if not status == "Quit":
        obj.notify("Power monitor ending")


class Monitor:
    def __init__(self):
        self.stdin_path="/dev/null"
        self.stdout_path="/var/log/powmon.out"
        self.stderr_path="/var/log/powmon.err"
        self.pidfile_path="/var/run/pm.pid"
        self.pidfile_timeout=5

    def run(self):
        monitor()
        
if __name__=="__main__":
    try:
        app = Monitor()
        dr=runner.DaemonRunner(app)
        dr.do_action()
    except KeyboardInterrupt:
        exit(0)

    
